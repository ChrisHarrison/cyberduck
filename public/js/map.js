var initialPoint = new google.maps.LatLng(51.644815, -0.298263);
var mapOptions = {
  zoom: 12,
  center: initialPoint
};
var map = new google.maps.Map(document.getElementById("map"), mapOptions);

google.maps.event.addListener(map, "idle", function() {

	var bounds = map.getBounds();

	//Request for properties which fall within the bounds of the map
	$.ajax({
		url: "map/data",
		method: "GET",
		data: {
			"northEastLat": bounds.getNorthEast().lat(),
			"northEastLng": bounds.getNorthEast().lng(),
			"southWestLat": bounds.getSouthWest().lat(),
			"southWestLng": bounds.getSouthWest().lng()
		}
	})
	.done(function(response) {
		plotPropertiesOnMap(response);
	});

});

var plotPropertiesOnMap = function(properties) {
	$.each(properties, function(k, property) {
		var infowindow = new google.maps.InfoWindow({
		    content: property.name
		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(property.latitude, property.longitude),
			map: map,
			title: property.name
		});
		marker.addListener("click", function() {
			infowindow.open(map, marker);
		});
	});
}