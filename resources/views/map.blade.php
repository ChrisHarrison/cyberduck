<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Properties Map</title>
    <link href="{!! asset('css/map.css') !!}" rel="stylesheet">
</head>
<body>
    <div id="map"></div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBbLRyePcHx4ovJ4i9FRgt7zBKAubcfrE0"></script>
    <script src="{!! asset('js/map.js') !!}"></script>
</body>
</html>