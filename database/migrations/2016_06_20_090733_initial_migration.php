<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialMigration extends Migration
{

    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('api_token', 32)->unique();
        });

        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->integer('user_id')->unsigned()->index();
            $table->decimal('latitude', 10, 8);
            $table->decimal('longitude', 11, 8);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('api_token');
        });
        Schema::drop('properties');
    }
}
