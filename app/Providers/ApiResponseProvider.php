<?php

namespace App\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class ApiResponseProvider extends ServiceProvider
{

    public function boot()
    {
        Response::macro('api', function($status, $data=null, $customStatus=null) {

            if ($status === true)
            {
                $response = [
                    'status' => true,
                    'response' => $data
                ];
                $statusCode = 200;
            }
            else {
                $response = [
                    'status' => false,
                    'errors' => $data
                ];
                $statusCode = 400;
            }

            if ($customStatus !== null) {
                $statusCode = $customStatus;
            }

            return response($response, $statusCode)
                    ->header('Content-Type', 'application/json');

        });
    }

    public function register()
    {
        //
    }

}
