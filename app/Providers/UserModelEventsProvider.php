<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use App\Property;

class UserModelEventsProvider extends ServiceProvider
{

    public function boot()
    {
        User::created(function ($object) {

            $properties = [];

            //3 random initial properties for each user created
            for ($i = 1; $i <= 5; $i++) {
                $properties[] = [
                    'name' => 'Property '.strtoupper(str_random(6)),
                    'user_id' => $object->id,
                    'latitude' => '51.'.mt_rand(567754, 684738),
                    'longitude' => '-0.'.mt_rand(173279, 437391),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=> date('Y-m-d H:i:s')
                ];
            }

            Property::insert($properties);

        });
    }

    public function register()
    {
        //
    }
}