<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;

abstract class Request extends FormRequest
{
    
	public function response(array $errors)
    {
        return response()->api(false, $errors);
    }

    public function forbiddenResponse()
    {
    	return response()->api(false, ['Forbidden'], 403);
    }

}
