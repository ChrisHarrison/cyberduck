<?php

namespace App\Http\Requests;

use Illuminate\Auth\AuthManager as Auth;
use App\Property;

class StorePropertyRequest extends Request
{

    public function authorize(Auth $auth)
    {

        //Check the property's existence
        $property = Property::find($this->get('id'));

        if (empty($property)) {
            //Trying to update a non-existent property
            abort(404);
        }

        if ($auth->check() && $property->user_id == $auth->id()) {
            //Current user owns the property
            return true;
        }

        return false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'name' => 'max:50',
            'latitude' => 'numeric',
            'longitude' => 'numeric'
        ];
    }

}
