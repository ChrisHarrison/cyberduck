<?php

//Non-protected API routes
Route::group([
	'prefix' => 'api/v1'
], function () {

	Route::post('user/login', 'UserController@login');
	Route::resource('user', 'UserController', ['only' => ['store']]);
	Route::resource('property', 'PropertyController', ['only' => ['index']]);

});

//Protected API routes
Route::group([
	'prefix' => 'api/v1',
	'middleware' => 'auth:api'
], function () {

	Route::resource('property', 'PropertyController', ['only' => ['store']]);

});

Route::get('map', 'MapController@index');
Route::get('map/data', 'MapController@data');