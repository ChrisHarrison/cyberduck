<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePropertyRequest;
use Illuminate\Auth\AuthManager as Auth;
use App\Property;
use App\User;

class PropertyController extends Controller
{
    public function index(Request $request)
    {

    	$query = Property::select('*');

    	if($request->has('user_id')) {

    		//Filter results by user
    		$query = $query->where('user_id', '=', $request->get('user_id'));

    	}
		if($request->has('radius') && $request->has('latitude') && $request->has('longitude')) {

			$latitude = (float) $request->get('latitude');
			$longitude = (float) $request->get('longitude');
			$radius = (float) $request->get('radius');

			$query = $query->radius($latitude, $longitude, $radius);
			$query->orderBy('distance');

		}

		return response()->api(true, $query->get());
    }

    public function store(StorePropertyRequest $request, Auth $auth)
    {
        $property = Property::findOrNew($request->get('id'));
        $property->fill($request->all());
    	$property->user_id = $auth->id();
        $save = $property->save();

    	if($save) {
            return response()->api(true, $property);
        }
        else {
            return response()->api(false, ['Database error']);
        }
    }

}
