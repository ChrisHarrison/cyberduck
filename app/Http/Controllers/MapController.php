<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;

class MapController extends Controller
{
    public function index(Request $request)
    {
        return view('map');
    }

    public function data(Request $request)
    {

        $northEastLat = (float) $request->get('northEastLat');
        $northEastLng = (float) $request->get('northEastLng');
        $southWestLat = (float) $request->get('southWestLat');
        $southWestLng = (float) $request->get('southWestLng');

        $properties = Property::withinBounds($northEastLat, $northEastLng, $southWestLat, $southWestLng)->get();

        return response()->json($properties);
    }

}
