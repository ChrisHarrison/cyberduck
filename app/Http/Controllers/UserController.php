<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\LoginRequest;
use App\User;
use Illuminate\Auth\AuthManager as Auth;

class UserController extends Controller
{
    
	public function store(StoreUserRequest $request)
	{
		$user = new User;
		$user->fill($request->all());

		$save = $user->save();

		if($save) {
			return response()->api(true, $user->highVisibility());
		}
		else {
			return response()->api(false, []);
		}

	}

	public function login(LoginRequest $request, Auth $auth)
	{

		$attempt = $auth->guard('web')->once([
			'email' => $request->get('email'),
			'password' => $request->get('password')
		]);

		if($attempt) {
			return response()->api(true, $auth->guard('web')->user()->highVisibility());
		}
		else {
			return response()->api(false, ['Invalid credentials']);
		}

	}

}
