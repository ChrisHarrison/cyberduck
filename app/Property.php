<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Property extends Model
{

    protected $fillable = [
        'name', 'latitude', 'longitude',
    ];

    public function user()
    {
        return $this->belongsTo('App/User');
    }

    public function scopeRadius($query, $latitude, $longitude, $distance)
    {
        $query = $query->select(\DB::raw('*, ( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'));
        $query = $query->having('distance', '<', $distance);
        return $query;
    }

    public function scopeWithinBounds($query, $northEastLat, $northEastLng, $southWestLat, $southWestLng)
    {
        $query = $query->where('latitude', '>', $southWestLat)
                        ->where('latitude', '<', $northEastLat)
                        ->where('longitude', '>', $southWestLng)
                        ->where('longitude', '<', $northEastLng);
        return $query;
    }
 
}
