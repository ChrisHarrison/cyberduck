<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class User extends Authenticatable
{

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    public function __construct()
    {
        $this->api_token = str_random(32);
    }

    public function properties()
    {
        return $this->hasMany('App/Property');
    }

    public static function create(array $attributes=[])
    {
        if(isset($attributes['password'])) {
            $attributes['password'] = Hash::make($attributes['password']);
        }
        return parent::create($attributes);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function highVisibility() {
        $this->hidden = [];
        return $this;
    }

}
